<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'first_name' => 'Super',
                'last_name' =>  'Admin',
                'email'     => 'superadmin@superadmin.com',
                'password'  => bcrypt('superadmin'),
                'user_role_idFk' => 1,
                'image' => 'default.jpg',
                'created_by' => 1
            ],[
                'first_name' => 'Admin',
                'last_name' => 'Kashif',
                'email'     => 'admin@admin.com',
                'password'  => bcrypt('admin'),
                'user_role_idFk' => 2,
                'image' => 'default.jpg',
                'created_by' => 1
            ],[
                'first_name' => 'User',
                'last_name' => 'Ali',
                'email'     => 'user@user.com',
                'password'  => bcrypt('user'),
                'user_role_idFk' => 3,
                'image' => 'default.jpg',
                'created_by' => 2
            ]
        ]);

        

    }
}
