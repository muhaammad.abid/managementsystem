<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::group(['middlewareGroups' => 'web'], function () {

    // For AuthController
    Route::get('logout', 'Auth\LoginController@logout');

    Route::group(['middleware' => 'unAuthenticated'], function() {

        Route::get('/', function () {
            return redirect('login');
        });

        Route::get('login', [ 'as' => 'login', 'uses' => 'Auth\LoginController@login_page']);
        Route::get('forgetpassword', 'Auth\LoginController@forget_page');
        Route::post('authuser', 'Auth\LoginController@authenticateUser');
        Route::post('forget/submit', 'Auth\LoginController@forget_submit');

    });

    // admin area
    Route::group(['middleware' => 'adminAuth'], function() {

        Route::get('admin/dashboard', 'AdminController@dashboard');
        
    });
    
    

    // user area
    Route::group(['middleware' => 'user'], function() {


    });


    



});

