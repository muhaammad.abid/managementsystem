<!DOCTYPE html>
<html lang="en">
	<head>
		<title> Admin - @yield('title')</title>

		@include('admin.master.head')


	</head>
	<body>
		<!--Preloader-->
		<div class="preloader-it">
			<div class="la-anim-1"></div>
		</div>
		<!--/Preloader-->

		<div class="wrapper theme-1-active pimary-color-green">
			@include('admin.master.header')

			@include('admin.master.sidebar')

			<!-- Right Sidebar Backdrop -->
			<div class="right-sidebar-backdrop"></div>
			<!-- /Right Sidebar Backdrop -->
				<!-- Main Content -->
				<div class="page-wrapper">

					@yield('content')

					@include('admin.master.footer')
				</div>

			
		</div>

		@section('foot')
			@include('admin.master.foot')


		@show


	</body>
</html>